extends TextEdit


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var inputTable = {}
var jisyoTable = {}
var regex = RegEx.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	var file = File.new()
	file.open("res://input.json", file.READ)
	var file_text = file.get_as_text()
	file.close()
	var json_parse = JSON.parse(file_text)
	inputTable = json_parse.result
	
	var file2 = File.new()
	file2.open("res://SKK-JISYO.S", file.READ)
	var file2_text = file2.get_as_text()
	file2.close()
	jisyoTable = parseJisyo(file2_text)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event.is_action_released("editor_emmet"):
		emmet()


func emmet():
	var buff = text.replace("\t", "")
	var command = detectCommand(buff)
	match command:
		"/":
			buff = swapExchangeSelect(buff)
		" ":
			if(checkExchange(buff)):
				buff = selectExchange(buff, 1)
			else:
				buff = toKana(buff.substr(0, buff.length() - 1), 0, true)
		"0","1","2","3","4","5","6","7","8","9":
			buff = selectExchange(buff, command as int)
		"q":
			buff = toKana(buff, 1, false)
		_:
			buff = toKana(buff, 0, false)
	text = buff
	cursor_set_column(text.length())


func checkExchange(txt : String) -> bool:
	return txt.length() > 1 and txt[txt.length() - 2] == "/"

func selectExchange(txt : String, index : int) -> String:
	if txt.length() <= 2:
		return txt
	var buff = txt.substr(0, txt.length() - 2)
	var columns = buff.split("/")
	if columns.size() <= 2:
		return txt
	if columns.size() <= index:
		return buff
	if index <= 0:
		index = columns.size() - 1
	return columns[0] + columns[index]

func exchange(txt : String) -> String:
	return ""

func toKana(txt : String, mode : int, jisyo : bool) -> String:
	var blocks = detectProcessing(txt)
	var buff = blocks[1]
	if mode == 1 and buff.length() >= 2:
		buff = buff.substr(0, buff.length() - 1)
	var tokens = tokenize(buff)
	var result = ""
	for token in tokens:
		if inputTable.has(token):
			result += inputTable[token][mode]
		else:
			if inputTable.has(token.right(1)):
				if token.left(1) == "n":
					result += inputTable["n"][mode]
				elif token.left(1) == "x":
					result += inputTable["x"][mode]
				else:
					result += inputTable["t"][mode]
				result += inputTable[token.right(1)][mode]
			else:
				result += token
	if jisyo and jisyoTable.has(result):
		return blocks[0] + jisyoTable[result]
	return blocks[0] + result

func tokenize(txt : String) -> Array:
	var tokens = []
	var cursol = 0
	for i in range(txt.length()):
		match txt[i]:
			"a", "i", "u", "e", "o":
				tokens.append(txt.substr(cursol, i - cursol + 1))
				cursol = i + 1
		if i == txt.length() - 1 and i >= cursol:
			tokens.append(txt.substr(cursol, i - cursol + 1))
	return tokens

func detectProcessing(txt : String) -> Array:
	var head = ""
	var tail = txt
	for i in range(txt.length() - 1, -1, -1):
		if txt[i] < "a" or "z" < txt[i]:
			head = txt.substr(0, i + 1)
			tail = txt.substr(i + 1)
			break
	if head.length() > 0 and head[head.length() - 1] == " ":
		head = head.substr(0, head.length() - 1)
	return [head, tail]

func detectCommand(txt : String) -> String:
	if txt.length() > 0:
		return txt[-1]
	return ""

func parseJisyo(txt : String):
	var result = {}
	regex.compile("^(.+) \\/(.+)\\/$")
	for line in txt.split("\n"):
		var matched = regex.search(line)
		if matched:
			var henkango = matched.get_string(2)
			if henkango.find("/") >= 0:
				henkango = "/%s/" % henkango
			result[matched.get_string(1)] = henkango
	return result

func swapExchangeSelect(txt : String) -> String:
	var frontMode = true
	var buff = txt.substr(0, txt.length() - 1)
	if txt.length() > 1 and txt[txt.length() - 2] == "/":
		frontMode = false
		buff = txt.substr(0, txt.length() - 2)
	var splitted = buff.split("/")
	if splitted.size() > 2:
		var base = splitted[0]
		if frontMode:
			var poped = splitted[1]
			splitted.remove(1)
			splitted.append(poped)
		else:
			var poped = splitted[splitted.size() - 1]
			splitted.remove(splitted.size() - 1)
			splitted.insert(1, poped)
		
		buff = splitted.join("/") + "/"
	return buff
